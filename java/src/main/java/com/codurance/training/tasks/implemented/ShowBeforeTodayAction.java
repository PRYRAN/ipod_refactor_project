package com.codurance.training.tasks.implemented;

import com.codurance.training.tasks.Task;
import com.codurance.training.tasks.context.BaseContextWithRouter;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

public class ShowBeforeTodayAction extends ShowTasksAcliction {

    @Override
    public String message() {
        return "";
    }

    @Override
    public String title() {
        return "today";
    }

    @Override
    public void execute(BaseContextWithRouter context) {

        BaseContextWithRouter.Container container = context.content();

        LocalDate todayDate = LocalDate.now();
        List<Task> tasksBeforetoday = container.getTaskIdToTaskMap().values().stream()
                .filter(task -> task.getDeadline() != null)
                .filter(task -> task.getDeadline().isBefore(todayDate)).collect(Collectors.toList());
        show(tasksBeforetoday);
    }
}
