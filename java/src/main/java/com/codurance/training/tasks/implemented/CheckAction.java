package com.codurance.training.tasks.implemented;

import com.codurance.training.tasks.Reader;
import com.codurance.training.tasks.Task;
import com.codurance.training.tasks.context.BaseContextWithRouter;

import java.util.List;
import java.util.Map;

public class CheckAction extends SetStatusAction {
    @Override
    public String message() {
        return "please, write\n<TASK_ID>";
    }

    @Override
    public String title() {
        return "check";
    }

    @Override
    public void execute(BaseContextWithRouter context) {


        BaseContextWithRouter.Container container = context.content();
        Reader reader = container.getReader();
        Map<String, List<Task>> projectToTaskMap = container.getProjectToTaskMap();
        Map<String, Task> taskIdToTaskMap = container.getTaskIdToTaskMap();

        String taskId = reader.readString();

        Task task = taskIdToTaskMap.get(taskId);
        if (task == null){
            System.out.println("no cuch task");
        }else {
            setStatus(task,true);
        }



    }
}
