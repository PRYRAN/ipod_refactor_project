package com.codurance.training.tasks.implemented;

import com.codurance.training.tasks.Action;
import com.codurance.training.tasks.Reader;
import com.codurance.training.tasks.Task;
import com.codurance.training.tasks.context.BaseContextWithRouter;

import java.util.List;
import java.util.Map;

public class AddTaskAction implements Action<BaseContextWithRouter> {
    @Override
    public String message() {
        return "please, write\n<PROJECT_NAME>\n<DESCRIPTION>\n<ID>";
    }

    @Override
    public String title() {
        return "add task";
    }

    @Override
    public void execute(BaseContextWithRouter context) {

        BaseContextWithRouter.Container container = context.content();
        Reader reader = container.getReader();
        Map<String, List<Task>> projectToTaskMap = container.getProjectToTaskMap();
        Map<String, Task> taskIdToTaskMap = container.getTaskIdToTaskMap();

        String project = null;
        String description = null;
        String id = null;

        project = reader.readString();
        description = reader.readString();
        id = reader.readString();

        // TODO: 15.12.2020 dissallow spaces
        if (!id.matches("[a-zA-Z0-9]")){
            System.out.println("cpecial characters are nor allowed");
            return;
        }


        List<Task> projectTasks = projectToTaskMap.get(project);
        if (projectTasks == null) {
            System.out.printf("Could not find a project with the name \"%s\".", project);
            System.out.println();
            return;
        }


        Task task;
        if (id == null) {
            task = new Task(Task.getNextId().toString(), description, project,false);
        } else {
            task = new Task(id, description, project,false);
        }


        try {
            projectTasks.add(task);
            taskIdToTaskMap.put(task.getId(), task);
        } catch (Exception e) {
            System.out.println("error");
        }
    }
}
