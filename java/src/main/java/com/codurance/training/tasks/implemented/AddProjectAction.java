package com.codurance.training.tasks.implemented;

import com.codurance.training.tasks.Action;
import com.codurance.training.tasks.Reader;
import com.codurance.training.tasks.Task;
import com.codurance.training.tasks.context.BaseContextWithRouter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AddProjectAction implements Action<BaseContextWithRouter> {
    @Override
    public String message() {
        return "please, write\n<PROJECT_NAME>";

    }

    @Override
    public String title() {
        return "add project";
    }

    @Override
    public void execute(BaseContextWithRouter context) {


        BaseContextWithRouter.Container container = context.content();
        Reader reader = container.getReader();
        Map<String, List<Task>> projectToTaskMap = container.getProjectToTaskMap();
        Map<String, Task> taskIdToTaskMap = container.getTaskIdToTaskMap();

        String projectName = reader.readString();

        projectToTaskMap.put(projectName,new ArrayList<>());

    }
}
