package com.codurance.training.tasks.context;




public interface Context <E> {

    E content();

}
