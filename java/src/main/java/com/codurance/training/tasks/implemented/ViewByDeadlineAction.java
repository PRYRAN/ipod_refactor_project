package com.codurance.training.tasks.implemented;

import com.codurance.training.tasks.Action;
import com.codurance.training.tasks.Reader;
import com.codurance.training.tasks.Task;
import com.codurance.training.tasks.TaskList;
import com.codurance.training.tasks.context.BaseContextWithRouter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ViewByDeadlineAction extends ShowTasksAcliction {
    @Override
    public String message() {
        return String.format("please, write\n <DATA> in fortmat %s", Reader.DATE_FORMAT);
    }

    @Override
    public String title() {
        return "view by deadline";
    }

    @Override
    public void execute(BaseContextWithRouter context) {




        BaseContextWithRouter.Container container = context.content();
        Reader reader = container.getReader();
        Map<String, List<Task>> projectToTaskMap = container.getProjectToTaskMap();
        Map<String, Task> taskIdToTaskMap = container.getTaskIdToTaskMap();

        LocalDate date = reader.readLocalDate();

        List<Task> tasks = new ArrayList<>(taskIdToTaskMap.values()).stream()
                .filter(task -> task.getDeadline().isBefore(date)).collect(Collectors.toList());

        show(tasks);

    }
}
