package com.codurance.training.tasks;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Optional;

public final class Task {
    private final String id;
    private final String description;
    private String project;
    private Boolean isDone;
    private LocalDate deadline;

    private static Long idGenerator = 0L;

    public Task(String id, String description, String project, Boolean isDone) {
        this.id = id;
        this.description = description;
        this.project = project;
        this.isDone = isDone;
    }

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public String getProject() {
        return project;
    }

    public Boolean getDone() {
        return isDone;
    }

    public LocalDate getDeadline() {
        return deadline;
    }

    public static Long getIdGenerator() {
        return idGenerator;
    }

    public void setDeadline(LocalDate deadline) {
        this.deadline = deadline;
    }

    public void setDone(Boolean done) {
        isDone = done;
    }

    public static Long getNextId(){
        return idGenerator++;
    }


}
