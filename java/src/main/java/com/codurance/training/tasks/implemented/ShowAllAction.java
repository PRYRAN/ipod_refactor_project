package com.codurance.training.tasks.implemented;

import com.codurance.training.tasks.Action;
import com.codurance.training.tasks.Task;
import com.codurance.training.tasks.context.BaseContextWithRouter;

import java.util.ArrayList;

public class ShowAllAction extends ShowTasksAcliction {
    @Override
    public String message() {
        return "";
    }

    @Override
    public String title() {
        return "show";
    }

    @Override
    public void execute(BaseContextWithRouter context) {
        BaseContextWithRouter.Container container = context.content();

        show(new ArrayList<Task>(container.getTaskIdToTaskMap().values()));

    }
}
