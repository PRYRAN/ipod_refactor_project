package com.codurance.training.tasks.implemented;


import com.codurance.training.tasks.Action;
import com.codurance.training.tasks.ActionType;
import com.codurance.training.tasks.Router;
import com.codurance.training.tasks.context.BaseContextWithRouter;

public class ExitAction implements Action<BaseContextWithRouter> {
    @Override
    public String message() {
        return "";
    }

    @Override
    public String title() {
        return "Exit";
    }

    @Override
    public void execute(BaseContextWithRouter context) {
        Router router = context.content().getRouter();

        ActionType type = router.getParrentOf(router.getCurrentAction());
        if (type == null){
            System.exit(0);
        }else {
            router.setCurrentAction(type);
        }
    }

}
