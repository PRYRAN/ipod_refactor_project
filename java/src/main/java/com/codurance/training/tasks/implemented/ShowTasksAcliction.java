package com.codurance.training.tasks.implemented;

import com.codurance.training.tasks.Action;
import com.codurance.training.tasks.Task;
import com.codurance.training.tasks.context.BaseContextWithRouter;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class ShowTasksAcliction implements Action<BaseContextWithRouter> {


    protected void show(List<Task> tasks) {

        Map<String, List<Task>> projectTotask = tasks.stream().collect(Collectors.groupingBy(Task::getProject));

        for (Map.Entry<String, List<Task>> project : projectTotask.entrySet()) {
            System.out.println(project.getKey());
            for (Task task : project.getValue()) {
                System.out.printf("    [%c] %s: %s %s%n",
                        (task.getDone() ? 'x' : ' '), task.getId(), task.getDescription(), (task.getDeadline() == null ? "": " with deadline: " + task.getDeadline().toString()));
            }
            System.out.println();
        }
    }
}
