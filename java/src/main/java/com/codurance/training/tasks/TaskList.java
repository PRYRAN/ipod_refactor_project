package com.codurance.training.tasks;

import com.codurance.training.tasks.context.BaseContextWithRouter;
import com.codurance.training.tasks.context.Context;
import com.codurance.training.tasks.implemented.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public final class TaskList implements Runnable {

    private static final Map<String, List<Task>> projectToTaskMap = new LinkedHashMap<>();
    private static final Map<String, Task> taskIdToTaskMap = new LinkedHashMap<>();

    // scanner wrapper for input reading
    static Reader reader = Reader.getInstance();

    // router to manipulate actions and submenus
    static Router router = Router.getInstance();


    static {
        // init configuration of menus
        router.addRoute(ActionType.INIT, ActionType.SHOW_ALL_TASKS,ActionType.SHOW_BEFORE_TODAY,
                ActionType.ADD_TASK,ActionType.ADD_PROJECT,ActionType.CHECK,ActionType.UNCHECK,
                ActionType.SET_DEADLINE, ActionType.DELETE_TASK, ActionType.VIEW_BY_DATE, ActionType.VIEW_BY_DEADLINE, ActionType.VIEW_BY_PROJECT);


        router.setCurrentAction(ActionType.INIT);
    }


    /**
     * here is the main part:
     * i have implemented fully scalable and generic action menus. In order to create new actions - you implement interface Action<Context>
     *     with specific implementation of interface Context. Context is something like container, in whice all required params are passed to action. Also there is no restctions
     *     Context. You can implement context in every shape and types, but return all paarams wrapped in Container. Finally, you create new ENUM for new action
     *     After that, you follow rules and aztualize info about actions in these 3 maps.
     *     Everything is clear, i think.
     */
    static HashMap<ActionType, Action> actions = new HashMap<>();
    static {
        actions.put(ActionType.INIT,new InitAction());
        actions.put(ActionType.SHOW_ALL_TASKS,new ShowAllAction());
        actions.put(ActionType.SHOW_BEFORE_TODAY,new ShowBeforeTodayAction());
        actions.put(ActionType.ADD_TASK,new AddTaskAction());
        actions.put(ActionType.ADD_PROJECT,new AddProjectAction());
        actions.put(ActionType.CHECK,new CheckAction());
        actions.put(ActionType.UNCHECK,new UncheckAction());
        actions.put(ActionType.SET_DEADLINE,new SetDeadlineAction());
        actions.put(ActionType.DELETE_TASK,new DeleteTaskAction());
        actions.put(ActionType.VIEW_BY_DATE,new ViewByDateAction());
        actions.put(ActionType.VIEW_BY_DEADLINE,new ViewByDeadlineAction());
        actions.put(ActionType.VIEW_BY_PROJECT,new ViewByProject());


        actions.put(ActionType.EXIT,new ExitAction());
    }

    static HashMap<Class<? extends Action>, ActionType> types = new HashMap<>();
    static {
        types.put(InitAction.class,ActionType.INIT);
        types.put(ShowAllAction.class,ActionType.SHOW_ALL_TASKS);
        types.put(ShowBeforeTodayAction.class,ActionType.SHOW_BEFORE_TODAY);
        types.put(AddTaskAction.class,ActionType.ADD_TASK);
        types.put(AddProjectAction.class,ActionType.ADD_PROJECT);
        types.put(CheckAction.class,ActionType.CHECK);
        types.put(UncheckAction.class,ActionType.UNCHECK);
        types.put(SetDeadlineAction.class,ActionType.SET_DEADLINE);
        types.put(DeleteTaskAction.class,ActionType.DELETE_TASK);
        types.put(ViewByDateAction.class,ActionType.VIEW_BY_DATE);
        types.put(ViewByDeadlineAction.class,ActionType.VIEW_BY_DEADLINE);
        types.put(ViewByProject.class,ActionType.VIEW_BY_PROJECT);


        types.put(ExitAction.class,ActionType.EXIT);
    }


    static HashMap<ActionType, Context> contextx = new HashMap<>();
    static {
        contextx.put(ActionType.INIT, new BaseContextWithRouter(reader,router,projectToTaskMap,taskIdToTaskMap));
        contextx.put(ActionType.SHOW_ALL_TASKS, new BaseContextWithRouter(reader,router,projectToTaskMap,taskIdToTaskMap));
        contextx.put(ActionType.SHOW_BEFORE_TODAY, new BaseContextWithRouter(reader,router,projectToTaskMap,taskIdToTaskMap));
        contextx.put(ActionType.ADD_TASK, new BaseContextWithRouter(reader,router,projectToTaskMap,taskIdToTaskMap));
        contextx.put(ActionType.ADD_PROJECT, new BaseContextWithRouter(reader,router,projectToTaskMap,taskIdToTaskMap));
        contextx.put(ActionType.CHECK, new BaseContextWithRouter(reader,router,projectToTaskMap,taskIdToTaskMap));
        contextx.put(ActionType.UNCHECK, new BaseContextWithRouter(reader,router,projectToTaskMap,taskIdToTaskMap));
        contextx.put(ActionType.SET_DEADLINE, new BaseContextWithRouter(reader,router,projectToTaskMap,taskIdToTaskMap));
        contextx.put(ActionType.DELETE_TASK, new BaseContextWithRouter(reader,router,projectToTaskMap,taskIdToTaskMap));
        contextx.put(ActionType.VIEW_BY_DATE, new BaseContextWithRouter(reader,router,projectToTaskMap,taskIdToTaskMap));
        contextx.put(ActionType.VIEW_BY_DEADLINE, new BaseContextWithRouter(reader,router,projectToTaskMap,taskIdToTaskMap));
        contextx.put(ActionType.VIEW_BY_PROJECT, new BaseContextWithRouter(reader,router,projectToTaskMap,taskIdToTaskMap));


        contextx.put(ActionType.EXIT, new BaseContextWithRouter(reader,router,projectToTaskMap,taskIdToTaskMap));

    }

    public static void main(String[] args) throws Exception {
        new TaskList().run();
    }

    public TaskList() {
    }

    public void run() {

        int choise;
        do {
            List<Action> choises = router.getCurrentChilds().stream().map(action ->
                    actions.get(action)).collect(Collectors.toList());
            choises.add(actions.get(ActionType.EXIT));

            choise = reader.getChoise(choises.stream().map(action -> action.title()).collect(Collectors.toList()));


            Action action = choises.get(choise);
            System.out.println(action.message());

            action.execute(contextx.get(types.get(action.getClass())));

        } while (true);
    }
}
