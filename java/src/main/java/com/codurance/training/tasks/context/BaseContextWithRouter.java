package com.codurance.training.tasks.context;


import com.codurance.training.tasks.Reader;
import com.codurance.training.tasks.Router;
import com.codurance.training.tasks.Task;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class BaseContextWithRouter implements Context<BaseContextWithRouter.Container> {


    Container container;

    public BaseContextWithRouter(Reader reader, Router router, Map<String, List<Task>> projectToTaskMap, Map<String, Task> taskIdToTaskMap){
        container = new Container(reader,router,projectToTaskMap,taskIdToTaskMap);
    }

    @Override
    public Container content() {
        return container;
    }


    public class Container{
        Reader reader;
        Router router;
        Map<String, List<Task>> projectToTaskMap;
        Map<String, Task> taskIdToTaskMap;

        public Container(Reader reader, Router router, Map<String, List<Task>> projectToTaskMap, Map<String, Task> taskIdToTaskMap) {
            this.reader = reader;
            this.router = router;
            this.projectToTaskMap = projectToTaskMap;
            this.taskIdToTaskMap = taskIdToTaskMap;
        }

        public Reader getReader() {
            return reader;
        }

        public Router getRouter() {
            return router;
        }

        public Map<String, List<Task>> getProjectToTaskMap() {
            return projectToTaskMap;
        }

        public Map<String, Task> getTaskIdToTaskMap() {
            return taskIdToTaskMap;
        }
    }
}
