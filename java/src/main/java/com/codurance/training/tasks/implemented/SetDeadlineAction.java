package com.codurance.training.tasks.implemented;

import com.codurance.training.tasks.Action;
import com.codurance.training.tasks.Reader;
import com.codurance.training.tasks.Task;
import com.codurance.training.tasks.TaskList;
import com.codurance.training.tasks.context.BaseContextWithRouter;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

public class SetDeadlineAction implements Action<BaseContextWithRouter> {
    @Override
    public String message() {
        return String.format("please, write\n<TASK_IT>\n<DAY> in format %s", Reader.DATE_FORMAT );

    }

    @Override
    public String title() {
        return "set deadline";
    }

    @Override
    public void execute(BaseContextWithRouter context) {




        BaseContextWithRouter.Container container = context.content();
        Reader reader = container.getReader();
        Map<String, List<Task>> projectToTaskMap = container.getProjectToTaskMap();
        Map<String, Task> taskIdToTaskMap = container.getTaskIdToTaskMap();


        String taskID = null;
        LocalDate deadline = null;
        try {
            taskID = reader.readString();
            deadline = reader.readLocalDate();
        } catch (Exception e){
            System.out.println("invadil number of inputs");
        }

        Task task = taskIdToTaskMap.get(taskID);
        if (task == null){
            System.out.println("no task found for that id");
            return;
        }

        task.setDeadline(deadline);
    }


}
