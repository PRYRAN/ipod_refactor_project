package com.codurance.training.tasks.implemented;

import com.codurance.training.tasks.Reader;
import com.codurance.training.tasks.Task;
import com.codurance.training.tasks.context.BaseContextWithRouter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ViewByProject extends ShowTasksAcliction{
    @Override
    public String message() {
        return "please, write\n <PROJECT>";
    }

    @Override
    public String title() {
        return "view by project";
    }

    @Override
    public void execute(BaseContextWithRouter context) {




        BaseContextWithRouter.Container container = context.content();
        Reader reader = container.getReader();
        Map<String, List<Task>> projectToTaskMap = container.getProjectToTaskMap();
        Map<String, Task> taskIdToTaskMap = container.getTaskIdToTaskMap();

        String project = reader.readString();

        if (!projectToTaskMap.containsKey(project)){
            System.out.println("no such project");
            return;
        }


        List<Task> tasks = new ArrayList<>(taskIdToTaskMap.values()).stream()
                .filter(task -> task.getProject().equals(project)).collect(Collectors.toList());

        show(tasks);

    }
}
