package com.codurance.training.tasks;


import com.codurance.training.tasks.context.Context;

public interface Action <CONTEXT extends Context> {

    String message();

    String title();

    void execute(CONTEXT context);



}
