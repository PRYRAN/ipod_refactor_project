package com.codurance.training.tasks.implemented;


import com.codurance.training.tasks.Action;
import com.codurance.training.tasks.context.Context;

public class InitAction implements Action {
    @Override
    public String message() {
        return "";
    }

    @Override
    public String title() {
        return "";
    }

    @Override
    public void execute(Context context) {

    }
}
