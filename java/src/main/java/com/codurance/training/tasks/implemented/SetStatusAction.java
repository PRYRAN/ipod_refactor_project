package com.codurance.training.tasks.implemented;

import com.codurance.training.tasks.Action;
import com.codurance.training.tasks.Task;
import com.codurance.training.tasks.context.BaseContextWithRouter;

public abstract class SetStatusAction implements Action<BaseContextWithRouter> {

    protected void setStatus(Task task, Boolean status){
        task.setDone(status);
    }
}
